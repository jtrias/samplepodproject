//
//  ViewController.m
//  SamplePodProject
//
//  Created by Juan on 16/03/14.
//  Copyright (c) 2014 IGZ. All rights reserved.
//

#import "ViewController.h"
#import <SamplePodLib/NSString+Normalize.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *unnormalizedStringTextField;
@property (weak, nonatomic) IBOutlet UITextField *normalizedStringTextField;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)normalize:(id)sender {
    _normalizedStringTextField.text =
    [_unnormalizedStringTextField.text normalizedString];
}

@end
